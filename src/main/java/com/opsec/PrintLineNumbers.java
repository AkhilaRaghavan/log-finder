package com.opsec;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class PrintLineNumbers {

    //A non word character
    public static final String NON_WORD_BOUNDARY = "\\b";

    public static void main(String[] args) throws URISyntaxException, IOException {
        List<String> allText = readFile("file1");
        Map<String, List<Integer>> nameToLineNumbers = new HashMap<>();

        // O(N) to read the names
        Map<String, Pattern> namesPattern = readFile("file2")
                 .stream()
                 .peek(name -> nameToLineNumbers.put(name, new ArrayList<>()))
                 .collect(Collectors.toMap(Function.identity(),
                         str -> Pattern.compile(NON_WORD_BOUNDARY + str + NON_WORD_BOUNDARY, Pattern.CASE_INSENSITIVE)));

        AtomicInteger lineNumber = new AtomicInteger(0);

        //O(N) to read each line
        allText.forEach(line -> {
            lineNumber.incrementAndGet();
            //O(M) to iterate the names set
            namesPattern.entrySet()
                    .stream()
                    .filter(entry -> entry.getValue().matcher(line).find())
                    .forEach(entry -> nameToLineNumbers.get(entry.getKey()).add(lineNumber.get()));
        });
        nameToLineNumbers.forEach((key, value) -> System.out.printf("%s : %s%n", key, value));
    }

    //This could be buffered stream reader, reading one line at a time.
    private static List<String> readFile(String resource) throws URISyntaxException, IOException {
        URL file1 = Thread.currentThread().getContextClassLoader().getResource(resource);
        assert file1 != null;
        return Files.readAllLines(Paths.get(file1.toURI()));
    }
}
