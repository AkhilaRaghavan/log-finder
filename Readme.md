Problem 1:
Input file 1 contains names and other strings.
Input file 2 contains a list of names, one name per line.



Take these 2 files as input and print to standard out the line numbers from file 1 where the name in file 2 is found. For example, if file 2 contains the name John, expected output would be John: 3, 46, 150. This means in file 1, the name John appears in the file on line 3, 46, 150.